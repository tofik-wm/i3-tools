Name:           i3-tools
Version:        1.0.1
Release:        2%{?dist}
Summary:        Set of scripts and tools for SwayWM configuration

License:        GPL
URL:            https://gitlab.com/tofik-rpms/i3-tools
Source0:        https://gitlab.com/tofik-wm/i3-tools/-/archive/v%{version}/i3-tools-v%{version}.tar.gz

BuildArch:      noarch

Requires:       bemenu
Requires:       (i3 or i3-gaps)

%description
%{summary}

%prep
%setup -q -n %{name}-v%{version}

%build


%install
install -D -pm 755 src/i3-confirm %{buildroot}%{_bindir}/i3-confirm

%check


%files
%license LICENSE
%{_bindir}/*
%changelog
* Wed Sep 14 2022 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.0.1-2
- Fixed dependencies

* Wed Sep 14 2022 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.0.1-1
- Added license

* Wed Sep 14 2022 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.0.0-1
- Initial build
